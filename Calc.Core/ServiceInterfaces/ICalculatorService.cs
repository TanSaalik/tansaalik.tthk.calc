﻿namespace Calc.Core.ApplicationServices
{
    public interface ICalculatorService
    {
        decimal EvalExpression(string expression);
    }
}
