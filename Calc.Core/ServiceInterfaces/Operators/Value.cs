﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calc.Core.ServiceInterfaces.Operators
{

    public class Value : IExpressionNode
    {
        public string StringValue { get; set; }

        public decimal NumericValue { get; set; }
    }
}
