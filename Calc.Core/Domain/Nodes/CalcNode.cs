﻿namespace Calc.Core.Domain
{
    public abstract class CalcNode
    {
        public abstract decimal GetValue();

        public abstract string GetString();
    }

}
