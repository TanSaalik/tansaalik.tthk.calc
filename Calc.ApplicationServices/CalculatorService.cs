﻿using Calc.Core.ApplicationServices;
using Calc.Core.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Calc.ApplicationServices
{
    public class Singleton
    {
        private static Singleton _instance;

        private Singleton()
        {
        }

        public static Singleton GetInstance()
        {
            return _instance ?? (_instance = new Singleton());
        }
    }

    public class CalculatorService : ICalculatorService
    {
        private readonly OperatorRegistry _opRegistry = new OperatorRegistry();

        public CalculatorService()
        {
            _opRegistry.RegOperator<BinaryCalcNode>(new AddOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new MultiplyOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new DivideOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new SubtractOperator());
        }

        public decimal EvalExpression(string expression)
        {
            var node = Parse(expression);
            return node.GetValue();
        }

        private TokenizeResult TokenizeInternal(string text, ref int i, int depth = 0)
        {
            var res = new TokenizeResult();

            if (string.IsNullOrWhiteSpace(text))
            {
                res.AddError("Empty input");
                return res;
            }

            var firstIndex = i;
            var firstChar = text[i];
            var isInParenthesis = false;
            if (depth > 0 && firstChar == '(')
            {
                i++;
                isInParenthesis = true;
            }

            var tokenList = new ArrayToken();

            var currentValue = string.Empty;

            for (; i < text.Length; ++i)
            {
                var c = text[i];

                if (c == '(')
                {
                    var innerExressionResult = TokenizeInternal(text, ref i, depth + 1);
                    res.Errors.AddRange(innerExressionResult.Errors);
                    tokenList.Add(new ArrayToken(innerExressionResult.Nodes));
                    continue;
                }
                if (c == ')')
                {
                    if (!isInParenthesis)
                    {
                        res.AddError("Opening parenthesis is missing", i);
                    }
                    else
                    {
                        CreateValueToken(i);

                        res.Nodes = tokenList.ToArray();
                        return res;
                    }
                }
                
                var opString = c.ToString();
                if (_opRegistry.IsKnowOperator(opString, out IOperator op))
                {
                    CreateValueToken(i);

                    if (op is IBinaryOperator && (tokenList.Count == 0 || tokenList.Last() is OperatorToken))
                    {
                        res.AddError("Left is missing", i);
                    }

                    tokenList.Add(new OperatorToken(op));

                    if (op is IBinaryOperator)
                    {
                        Action<Token> onNextTokenCreated = null;
                        var charIndex = i;
                        onNextTokenCreated = (token) =>
                        {
                            if (token is OperatorToken)
                            {
                                res.AddError("Right is missing", charIndex);
                            }

                            tokenList.TokenAdded -= onNextTokenCreated;
                        };

                        tokenList.TokenAdded += onNextTokenCreated;
                    }
                }
                else
                {
                    currentValue += c;
                }
            }

            CreateValueToken(i);

            if (isInParenthesis)
            {
                res.AddError("Missing end parenthesis", firstIndex);
            }

            void CreateValueToken(int charIndex)
            {
                if (string.IsNullOrWhiteSpace(currentValue))
                {
                    return;
                }

                var value = currentValue.Replace(',', '.');
                if (!decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out _))
                {
                    res.AddError("Invalid number", charIndex - currentValue.TrimStart().Length);
                }

                tokenList.Add(new ValueToken(currentValue.Trim()));

                currentValue = string.Empty;
            }

            res.Nodes = tokenList.GetTokens();
            return res;
        }

        public TokenizeResult Tokenize(string text)
        {
            var i = 0;
            return TokenizeInternal(text, ref i);
        }

        public CalcNode Parse(string v)
        {
            var tokenizeResult = Tokenize(v);
            return Parse(tokenizeResult.Nodes);
        }

        public CalcNode Parse(Token[] list)
        {
            OperatorToken highestNode = GetHighestNode(list, out var i);
            list = ProcessNode(list, highestNode, i);

            if (list.Length > 1)
            {
                return Parse(list);
            }

            return ((CalcNodeToken)list[0]).CalcNode;
        }

        public Token[] ProcessNode(Token[] list, OperatorToken highestNode, int i)
        {
            var prev = list[i - 1];
            var next = list[i + 1];

            if (highestNode.Operator is IBinaryOperator binary)
            {
                CalcNode left;
                CalcNode right;

                if (prev is ArrayToken arrayToken)
                {
                    left = Parse(arrayToken.GetTokens());
                }
                else
                {
                    left = prev is CalcNodeToken calcNodeToken
                        ? calcNodeToken.CalcNode
                        : new ValueCalcNode(((ValueToken)prev).Value);
                }

                if (next is ArrayToken arrayToken2)
                {
                    right = Parse(arrayToken2.GetTokens());
                }
                else
                {
                    right = next is CalcNodeToken calcNodeToken
                        ? calcNodeToken.CalcNode
                        : new ValueCalcNode(((ValueToken)next).Value);
                }

                var calcNode = new BinaryCalcNode(binary, left, right);

                return list.Replace(i - 1, i + 1, new CalcNodeToken(calcNode));
            }

            return null;
        }

        public OperatorToken GetHighestNode(Token[] list, out int highestNodeIndex)
        {
            highestNodeIndex = -1;
            OperatorToken highestNode = null;
            for (var i = 0; i < list.Length; i++)
            {
                if (list[i] is OperatorToken operatorToken)
                {
                    var isHighest = highestNode == null
                        || highestNode.Operator.Priority < operatorToken.Operator.Priority;
                    if (isHighest)
                    {
                        highestNode = operatorToken;
                        highestNodeIndex = i;
                    }
                }
            }

            return highestNode;
        }
    }

    public class TokenizeResult
    {
        public bool Success { get => (Errors?.Count ?? 0) == 0; }
        public List<TokenError> Errors { get; } = new List<TokenError>();
        public Token[] Nodes { get; set; }

        public void AddError(string error, int charIndex = 0)
        {
            Errors.Add(new TokenError
            {
                Error = error,
                CharIndex = charIndex
            });
        }
    }

    public class TokenError
    {
        public int CharIndex { get; set; }
        public string Error { get; set; }
    }

    public abstract class Token
    {
    }

    public class CalcNodeToken : Token 
    {
        public CalcNode CalcNode { get; }

        public CalcNodeToken(CalcNode calcNode)
        {
            CalcNode = calcNode;
        }
    }

    public class ValueToken : Token
    {
        public string Value { get; }

        public ValueToken(string value)
        {
            Value = value;
        }
    }

    public class OperatorToken : Token
    {
        public IOperator Operator { get; }

        public OperatorToken(IOperator @operator)
        {
            Operator = @operator;
        }
    }

    public class ArrayToken : Token, IEnumerable<Token>
    {
        public ArrayToken()
        {
        }

        public ArrayToken(IEnumerable<Token> tokens)
        {
            _tokens.AddRange(tokens);
        }

        private List<Token> _tokens = new List<Token>();
        public int Count => _tokens.Count;

        public void Add(Token token)
        {
            _tokens.Add(token);
            PublishTokenAdded(token);
        }

        private void PublishTokenAdded(Token token)
        {
            TokenAdded?.Invoke(token);
        }

        public event Action<Token> TokenAdded;

        public Token[] GetTokens()
        {
            return _tokens.ToArray();
        }

        public IEnumerator<Token> GetEnumerator()
        {
            return _tokens.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
