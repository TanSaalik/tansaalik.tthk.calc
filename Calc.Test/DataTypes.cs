﻿using Xunit;

namespace Calc.Test
{
    public class DataTypes
    {
        public abstract class Abstract
        {
            public abstract int Id { get; set; }

            public abstract void PrepareData();

            public void Calc()
            {
                PrepareData();
            }
        }

        public class TheClass : Abstract
        {
            private int _id;

            public override int Id
            {
                get => _id;
                set => _id = value;
            }

            public void DoSomething(TheStruct s)
            {
                s.Id = 10;
            }

            public override void PrepareData()
            {
                throw new System.NotImplementedException();
            }
        }

        public struct TheStruct
        {
            public int Id { get; set; }

            public void DoSomething(ref int i, TheClass c)
            {
                i = 10;
                c.Id = 10;
            }
        }

        [Fact]
        public void Test()
        {
            var classInstance = new TheClass();

            var structInstance = new TheStruct();

            var i = 1;
            structInstance.DoSomething(ref i, classInstance);

            classInstance.DoSomething(structInstance);

            Assert.Equal(10, i);
            Assert.Equal(10, classInstance.Id);
            Assert.Equal(0, structInstance.Id);

        }

    }
}
