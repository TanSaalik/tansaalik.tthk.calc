﻿using Calc.Core.ApplicationServices;
using Microsoft.AspNetCore.Mvc;

namespace Calc.Controllers
{
    public class CalculatorController : BaseApiController
    {
        private readonly ICalculatorService _calculatorService;

        public CalculatorController(ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        [HttpPost]
        //[Route("[action]/{expression}")]
        public IActionResult Calc([FromBody] string expression)
        {
            var res = _calculatorService.EvalExpression(expression);
            return Ok(res);
        }
    }
}
